﻿namespace FlowersDesktop
{
    public partial class AddEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.okButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.classLabel = new System.Windows.Forms.Label();
            this.comboBoxClass = new System.Windows.Forms.ComboBox();
            this.nameLabel = new System.Windows.Forms.Label();
            this.priceLabel = new System.Windows.Forms.Label();
            this.colorLabel = new System.Windows.Forms.Label();
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.priceTextBox = new System.Windows.Forms.TextBox();
            this.colorTextBox = new System.Windows.Forms.TextBox();
            this.materialLabel = new System.Windows.Forms.Label();
            this.bulbDiameterLabel = new System.Windows.Forms.Label();
            this.freshnessLabel = new System.Windows.Forms.Label();
            this.spikeLengthLabel = new System.Windows.Forms.Label();
            this.freshnessNumeric = new System.Windows.Forms.NumericUpDown();
            this.materialTextBox = new System.Windows.Forms.TextBox();
            this.spikeLengthTextBox = new System.Windows.Forms.TextBox();
            this.bulbDiameterTextBox = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.freshnessNumeric)).BeginInit();
            this.SuspendLayout();
            // 
            // okButton
            // 
            this.okButton.Location = new System.Drawing.Point(65, 235);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 23);
            this.okButton.TabIndex = 0;
            this.okButton.Text = "OK";
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(145, 235);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 1;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // classLabel
            // 
            this.classLabel.AutoSize = true;
            this.classLabel.Location = new System.Drawing.Point(2, 13);
            this.classLabel.Name = "classLabel";
            this.classLabel.Size = new System.Drawing.Size(38, 13);
            this.classLabel.TabIndex = 2;
            this.classLabel.Text = "Class: ";
            // 
            // comboBoxClass
            // 
            this.comboBoxClass.FormattingEnabled = true;
            this.comboBoxClass.Items.AddRange(new object[] {
            "Artificial",
            "Natural",
            "Rose",
            "Lily"});
            this.comboBoxClass.Location = new System.Drawing.Point(80, 10);
            this.comboBoxClass.Name = "comboBoxClass";
            this.comboBoxClass.Size = new System.Drawing.Size(167, 21);
            this.comboBoxClass.TabIndex = 3;
            this.comboBoxClass.SelectedIndexChanged += new System.EventHandler(this.comboBoxClass_SelectedIndexChanged);
            // 
            // nameLabel
            // 
            this.nameLabel.AutoSize = true;
            this.nameLabel.Location = new System.Drawing.Point(2, 46);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(41, 13);
            this.nameLabel.TabIndex = 4;
            this.nameLabel.Text = "Name: ";
            // 
            // priceLabel
            // 
            this.priceLabel.AutoSize = true;
            this.priceLabel.Location = new System.Drawing.Point(2, 80);
            this.priceLabel.Name = "priceLabel";
            this.priceLabel.Size = new System.Drawing.Size(37, 13);
            this.priceLabel.TabIndex = 5;
            this.priceLabel.Text = "Price: ";
            // 
            // colorLabel
            // 
            this.colorLabel.AutoSize = true;
            this.colorLabel.Location = new System.Drawing.Point(2, 111);
            this.colorLabel.Name = "colorLabel";
            this.colorLabel.Size = new System.Drawing.Size(37, 13);
            this.colorLabel.TabIndex = 6;
            this.colorLabel.Text = "Color: ";
            // 
            // nameTextBox
            // 
            this.nameTextBox.Location = new System.Drawing.Point(80, 43);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.Size = new System.Drawing.Size(166, 20);
            this.nameTextBox.TabIndex = 7;
            // 
            // priceTextBox
            // 
            this.priceTextBox.Location = new System.Drawing.Point(80, 77);
            this.priceTextBox.Name = "priceTextBox";
            this.priceTextBox.Size = new System.Drawing.Size(167, 20);
            this.priceTextBox.TabIndex = 8;
            this.priceTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.numbersTextBox_KeyPress);
            // 
            // colorTextBox
            // 
            this.colorTextBox.Location = new System.Drawing.Point(80, 108);
            this.colorTextBox.Name = "colorTextBox";
            this.colorTextBox.Size = new System.Drawing.Size(167, 20);
            this.colorTextBox.TabIndex = 9;
            // 
            // materialLabel
            // 
            this.materialLabel.AutoSize = true;
            this.materialLabel.Location = new System.Drawing.Point(2, 142);
            this.materialLabel.Name = "materialLabel";
            this.materialLabel.Size = new System.Drawing.Size(37, 13);
            this.materialLabel.TabIndex = 13;
            this.materialLabel.Text = "Material: ";
            // 
            // bulbDiameterLabel
            // 
            this.bulbDiameterLabel.AutoSize = true;
            this.bulbDiameterLabel.Location = new System.Drawing.Point(2, 173);
            this.bulbDiameterLabel.Name = "bulbDiameterLabel";
            this.bulbDiameterLabel.Size = new System.Drawing.Size(37, 13);
            this.bulbDiameterLabel.TabIndex = 16;
            this.bulbDiameterLabel.Text = "Bulb Diameter: ";
            // 
            // freshnessLabel
            // 
            this.freshnessLabel.AutoSize = true;
            this.freshnessLabel.Location = new System.Drawing.Point(2, 142);
            this.freshnessLabel.Name = "freshnessLabel";
            this.freshnessLabel.Size = new System.Drawing.Size(37, 13);
            this.freshnessLabel.TabIndex = 14;
            this.freshnessLabel.Text = "Freshness: ";
            // 
            // spikeLengthLabel
            // 
            this.spikeLengthLabel.AutoSize = true;
            this.spikeLengthLabel.Location = new System.Drawing.Point(2, 173);
            this.spikeLengthLabel.Name = "spikeLengthLabel";
            this.spikeLengthLabel.Size = new System.Drawing.Size(37, 13);
            this.spikeLengthLabel.TabIndex = 15;
            this.spikeLengthLabel.Text = "Spike Length: ";
            // 
            // freshnessNumeric
            // 
            this.freshnessNumeric.Location = new System.Drawing.Point(80, 139);
            this.freshnessNumeric.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.freshnessNumeric.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.freshnessNumeric.Name = "freshnessNumeric";
            this.freshnessNumeric.Size = new System.Drawing.Size(167, 20);
            this.freshnessNumeric.TabIndex = 16;
            this.freshnessNumeric.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // materialTextBox
            // 
            this.materialTextBox.Location = new System.Drawing.Point(80, 139);
            this.materialTextBox.Name = "materialTextBox";
            this.materialTextBox.Size = new System.Drawing.Size(167, 20);
            this.materialTextBox.TabIndex = 10;
            // 
            // spikeLengthTextBox
            // 
            this.spikeLengthTextBox.Location = new System.Drawing.Point(80, 170);
            this.spikeLengthTextBox.Name = "spikeLengthTextBox";
            this.spikeLengthTextBox.Size = new System.Drawing.Size(167, 20);
            this.spikeLengthTextBox.TabIndex = 11;
            this.spikeLengthTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.numbersTextBox_KeyPress);
            // 
            // bulbDiameterTextBox
            // 
            this.bulbDiameterTextBox.Location = new System.Drawing.Point(80, 170);
            this.bulbDiameterTextBox.Name = "bulbDiameterTextBox";
            this.bulbDiameterTextBox.Size = new System.Drawing.Size(167, 20);
            this.bulbDiameterTextBox.TabIndex = 12;
            this.bulbDiameterTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.numbersTextBox_KeyPress);
            // 
            // AddEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.colorTextBox);
            this.Controls.Add(this.priceTextBox);
            this.Controls.Add(this.nameTextBox);
            this.Controls.Add(this.colorLabel);
            this.Controls.Add(this.priceLabel);
            this.Controls.Add(this.nameLabel);
            this.Controls.Add(this.comboBoxClass);
            this.Controls.Add(this.classLabel);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.okButton);
            this.Name = "AddEditForm";
            this.Text = "AddEditForm";
            ((System.ComponentModel.ISupportInitialize)(this.freshnessNumeric)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Label classLabel;
        private System.Windows.Forms.ComboBox comboBoxClass;
        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.Label priceLabel;
        private System.Windows.Forms.Label colorLabel;
        private System.Windows.Forms.Label materialLabel;
        private System.Windows.Forms.Label freshnessLabel;
        private System.Windows.Forms.Label spikeLengthLabel;
        private System.Windows.Forms.Label bulbDiameterLabel;
        private System.Windows.Forms.TextBox nameTextBox;
        private System.Windows.Forms.TextBox priceTextBox;
        private System.Windows.Forms.TextBox colorTextBox;
        private System.Windows.Forms.TextBox materialTextBox;
        private System.Windows.Forms.TextBox spikeLengthTextBox;
        private System.Windows.Forms.TextBox bulbDiameterTextBox;
        private System.Windows.Forms.NumericUpDown freshnessNumeric;
    }
}