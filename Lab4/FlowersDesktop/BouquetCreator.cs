﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Flowers;
using Logic;

namespace FlowersDesktop
{
    public partial class BouquetCreator : Form
    {
        Bouquet bouquet;

        public BouquetCreator()
        {
            bouquet = BouqetFactory.CreateBouqet();
            InitializeComponent();
            UpdateBouquetList();
        }

        private void UpdateBouquetList()        //Обновляет содержимое элемента управления BouquetList
        {
            bouquetList.DataSource = null;
            bouquetList.DataSource = bouquet.ListOfFlowers;
            bouquetList.DisplayMember = "Name";
            UpdatePrice();
        }

        private void UpdatePrice()          //Обновляет цену
        {
            priceLabel.Text = "Total price: " + FlowersCalculator.GetTotalPrice(bouquet).ToString();
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            AddEditForm addEditForm = new AddEditForm();
            addEditForm.ShowDialog();
            if (addEditForm.Result == DialogResult.OK)
            {
                bouquet.AddFlower(addEditForm.Flower);
            }
            UpdateBouquetList();
        }

        private void editButton_Click(object sender, EventArgs e)
        {
            AbstractFlower flower = (AbstractFlower)bouquetList.SelectedItem;
            if (flower != null)
            {
                AddEditForm addEditForm = new AddEditForm(flower);
                addEditForm.ShowDialog();
                if (addEditForm.Result == DialogResult.OK)
                {
                    bouquet.RemoveFlower(flower);
                    bouquet.AddFlower(addEditForm.Flower);
                    UpdateBouquetList();
                }
            }
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            AbstractFlower flower = (AbstractFlower)bouquetList.SelectedItem;
            if (flower != null)
            {
                bouquet.RemoveFlower(flower);
                UpdateBouquetList();
            }
        }
    }
}
