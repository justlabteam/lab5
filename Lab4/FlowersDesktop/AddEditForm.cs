﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Flowers;
using Logic;

namespace FlowersDesktop
{
    public partial class AddEditForm : Form
    {
        private AbstractFlower _flower;
        private DialogResult _result;

        public AbstractFlower Flower
        {
            get { return _flower; }
            set { _flower = value; }
        }

        public DialogResult Result
        {
            get { return _result; }
            set { _result = value; }
        }

        public AddEditForm()
        {
            Flower = null;
            InitializeComponent();
        }

        public AddEditForm(AbstractFlower flower)       //Конструктор редактирования
        {
            InitializeComponent();
            Flower = null;
            if (flower != null)
            {
                AddElements(flower.GetType().ToString());
                FillElements(flower);
            }
        }

        private void AddElements(string type)          //Добавляет элементы управления на окно после выбора класса, либо при редактировании элемента
        {
            RemoveControls();
            switch (type)
            {
                case "Artificial": 
                case "Flowers.ArtificialFlower":
                    this.Controls.Add(materialLabel);
                    this.Controls.Add(materialTextBox);
                    break;
                case "Natural":
                case "Flowers.NaturalFlower":
                    this.Controls.Add(freshnessLabel);  
                    this.Controls.Add(freshnessNumeric);
                    break;
                case "Rose":
                case "Flowers.Rose":
                    this.Controls.Add(freshnessLabel);
                    this.Controls.Add(freshnessNumeric);
                    this.Controls.Add(spikeLengthLabel);
                    this.Controls.Add(spikeLengthTextBox);
                    break;
                case "Lily":
                case "Flowers.Lily":
                    this.Controls.Add(freshnessLabel);
                    this.Controls.Add(freshnessNumeric);
                    this.Controls.Add(bulbDiameterLabel);
                    this.Controls.Add(bulbDiameterTextBox);
                    break;
            }
        }

        private void FillElements(AbstractFlower flower)         //Заполняет элементы данными при редактировании уже существующего элемента
        {
            switch (flower.GetType().ToString())
            {
                case "Flowers.ArtificialFlower":
                    comboBoxClass.SelectedItem = "Artificial";
                    materialTextBox.Text = ((ArtificialFlower)flower).Material;
                    break;
                case "Flowers.NaturalFlower":
                    comboBoxClass.SelectedItem = "Natural";
                    freshnessNumeric.Value = ((NaturalFlower)flower).Freshness;
                    break;
                case "Flowers.Rose":
                    comboBoxClass.SelectedItem = "Rose";
                    freshnessNumeric.Value = ((Rose)flower).Freshness;
                    spikeLengthTextBox.Text = ((Rose)flower).SpikeLength.ToString();
                    break;
                case "Flowers.Lily":
                    comboBoxClass.SelectedItem = "Lily";
                    freshnessNumeric.Value = ((Lily)flower).Freshness;
                    bulbDiameterTextBox.Text = ((Lily)flower).BulbDiameter.ToString();
                    break;
            }
            nameTextBox.Text = flower.Name;
            colorTextBox.Text = flower.Color;
            priceTextBox.Text = flower.Price.ToString();
            Flower = flower;
        }

        private bool CheckFields()      //Проверяет, заполнены ли все поля. Если заполнены - возвращает true.
        {
            bool check_fields = true;
            foreach (Control control in this.Controls)
            {
                if (control is TextBox && (control as TextBox).Text == "")
                {
                    check_fields = false;
                }
            }
            return check_fields;
        }

        private void numbersTextBox_KeyPress(object sender, KeyPressEventArgs e)              //Событие для textbox, позволяющее вводить только цифры
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ','))
            {
                e.Handled = true;
            }
        }

        private void comboBoxClass_SelectedIndexChanged(object sender, EventArgs e) //Вызывается при изменении
        {
            AddElements(comboBoxClass.SelectedItem.ToString());
        }

        private void RemoveControls()               //Убирает все опциональные элементы управления с экрана
        {
            this.Controls.Remove(this.materialLabel);
            this.Controls.Remove(this.freshnessLabel);
            this.Controls.Remove(this.spikeLengthLabel);
            this.Controls.Remove(this.bulbDiameterLabel);
            this.Controls.Remove(this.materialTextBox);
            this.Controls.Remove(this.freshnessNumeric);
            this.Controls.Remove(this.spikeLengthTextBox);
            this.Controls.Remove(this.bulbDiameterTextBox);
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            if (!CheckFields())                 //Проверка на то, заполнены ли все поля
            {
                MessageBox.Show("All fields must have values!");
                return;
            }
            if (Convert.ToDouble(priceTextBox.Text) < 0)        //Проверка на отрицательное значение в поле цены
            {
                MessageBox.Show("Price can not be negative!");
                return;
            }
            if (comboBoxClass.SelectedItem != null)
            {
                switch (comboBoxClass.SelectedItem.ToString())
                {
                    case "Artificial":
                        Flower = FlowersFactory.CreateFlower("Artificial");
                        ((ArtificialFlower)Flower).Material = materialTextBox.Text;
                        break;
                    case "Natural":
                        Flower = FlowersFactory.CreateFlower("Natural");
                        ((NaturalFlower)Flower).Freshness = (int)freshnessNumeric.Value;
                        break;
                    case "Rose":
                        Flower = FlowersFactory.CreateFlower("Rose");
                        ((Rose)Flower).Freshness = (int)freshnessNumeric.Value;
                        ((Rose)Flower).SpikeLength = Convert.ToDouble(spikeLengthTextBox.Text);
                        break;
                    case "Lily":
                        Flower = FlowersFactory.CreateFlower("Lily");
                        ((Lily)Flower).Freshness = (int)freshnessNumeric.Value;
                        ((Lily)Flower).BulbDiameter = Convert.ToDouble(bulbDiameterTextBox.Text);
                        break;
                }
                if (Flower != null)
                {
                    Flower.Name = nameTextBox.Text;
                    Flower.Price = Convert.ToDouble(priceTextBox.Text);
                    Flower.Color = colorTextBox.Text;
                    Result = DialogResult.OK;
                    this.Close();
                }
            }
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            Result = DialogResult.Cancel;
            this.Close();
        }
    }
}
