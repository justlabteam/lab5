﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Logic;
using Flowers;

namespace Lab4
{
    class FlowersConsole
    {
        static void Main(string[] args)
        {
            Flowers.Bouquet bouquet = BouqetFactory.CreateBouqet();     //Собираем букет

            BouqetPrinter.Print(bouquet);                       //Выводим информацию о букете

            double price = FlowersCalculator.GetTotalPrice(bouquet);    //Находим цену букета

            Console.WriteLine("Price = " + price);              //Выводим цену

            Console.ReadKey();
        }
    }
}
